/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javax.swing.AbstractListModel;

/**
 *
 * @author bhp
 */
public class NumbersListModel extends AbstractListModel
{
    private Integer[] numbers;
    
    public NumbersListModel()
    {
        numbers = new Integer[1];
    }
    
    public Integer[] getElements()
    {
        return numbers;
    }
    
    @Override
    public int getSize()
    {
        return numbers.length;
    }

    @Override
    public Object getElementAt(int i)
    {
        return numbers[i];
    }
    
    public void setNumbers(Integer[] numbers)
    {
        this.numbers = numbers;
        fireContentsChanged(this, 0, numbers.length);
    }
   
}
