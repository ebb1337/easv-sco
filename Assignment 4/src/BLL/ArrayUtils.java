package BLL;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Emil Blücher Busch
 */

public class ArrayUtils
{
    private static Integer[] intArrToIntegerArr(int[] source)
    {
        ArrayList<Integer> result = new ArrayList<>();
        
        for(int i : source)
            result.add(i);
        
        return (Integer[])result.toArray();
    }
    
    public static <T, TResult> TResult fold(T[] source, TResult acc, 
            BiFunction<TResult, T, TResult> f)
    {
        for(T item : source)
            acc = f.apply(acc, item);
        
        return acc;
    }
    
    public static <T> T[] filter(T[] source, Function<T, Boolean> f)
    {
        return (T[])fold(source, new ArrayList<T>(), (acc, x) -> 
        {
            if(f.apply(x))
                acc.add(x);
            return acc;
        }).toArray();
    }
    
    public static <T> T[] init(int count, Class<T> c, Function<Integer, T> initalizer)
    {
        T[] arr = (T[])Array.newInstance(c, count);
        
        for(int i = 0; i < count; i++) 
            arr[i] = initalizer.apply(i);
        
        return arr;
    }
    
    public static <T> boolean isEmpty(Iterable<T> source) 
    {
        return source.iterator().hasNext();
    }
    
    public static int sum(Integer[] source)
    {
        return fold(source, 0, (acc, x) -> acc + x);
    }
    
    public static int sum(int[] source)
    {
        return sum(intArrToIntegerArr(source));
    }
    
    public static int min(Integer[] source)
    {
        return fold(source, 0, (acc, x) -> x < acc ? x : acc);
    }

    public static int min(int[] source)
    {
        return min(intArrToIntegerArr(source));
    }

    public static int max(Integer[] source)
    {
        return fold(source, 0, (acc, x) -> x > acc ? x : acc);
    }
    
    public static int max(int[] source)
    {
        return max(intArrToIntegerArr(source));
    }
    
    public static double average(Integer[] source)
    {
        return (double)sum(source) / source.length;
    }
    
    public static double average(int[] source)
    {
        return (double)sum(source) / source.length;
    }

    public static <T> int count(T[] source, Function<T, Boolean> f)
    {
        return filter(source, x -> f.apply(x)).length;
    }

    public static <T> int indexOf(T[] source, T itemToFind)
    {
        int i = 0; 
        
        for(T item : source)
            if(item.equals(itemToFind))
                return i;
            else i++;
        
        return -1;
    }

    public static <T> int lastIndexOf(T[] source, T itemToFind)
    {
        int i = -1;
        int temp = 0;
        
        for(T item : source)
        {
            if(item.equals(itemToFind))
                i = temp;
            temp++;
        }
        
        return i;
    }

    public static <T> T[] reverse(T[] source, Class<T> c)
    {
        for(int i = 0; i < source.length; i++)
            source[source.length - 1 - i] = source[i];
        
        return source;
    }
}