package BLL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Emil Blücher Busch
 */

public class IterableUtils
{
    public static <T, TResult> TResult fold(Iterable<T> source, TResult acc, 
            BiFunction<TResult, T, TResult> f)
    {
        for(T item : source)
            acc = f.apply(acc, item);
        
        return acc;
    }

    public static <T> Iterable<T> filter(Iterable<T> source, Function<T, Boolean> f)
    {
        return fold(source, new ArrayList<T>(), (acc, x) -> 
        {
            if(f.apply(x))
                acc.add(x);
            return acc;
        });
    }
    
    public static <T> Iterable<T> init(int count, Function<Integer, T> initalizer)
    {
        ArrayList<T> result = new ArrayList<>();
        
        for(int i = 0; i < count; i++) 
            result.add(initalizer.apply(i));
        
        return result;
    }
    
    public static <T> boolean isEmpty(Iterable<T> source) 
    {
        return source.iterator().hasNext();
    }
    
    public static <T> int size(Iterable<T> source)
    {
        return source instanceof Collection 
            ? ((Collection<T>)source).size() 
            : fold(source, 0, (acc, x) -> acc + 1);
    }
    
    public static int sum(Iterable<Integer> source)
    {
        return fold(source, 0, (acc, x) -> acc + x);
    }

    public static int min(Iterable<Integer> source)
    {
        return fold(source, 0, (acc, x) -> x < acc ? x : acc);
    }

    public static int max(Iterable<Integer> source)
    {
        return fold(source, 0, (acc, x) -> x > acc ? x : acc);
    }

    public static double average(Iterable<Integer> source)
    {
        return (double)sum(source) / size(source);
    }

    public static <T> int count(Iterable<T> source, Function<T, Boolean> f)
    {
        return size(filter(source, x -> f.apply(x)));
    }

    public static <T> int indexOf(Iterable<T> source, T itemToFind)
    {
        if(source instanceof ArrayList)
            return ((ArrayList<T>)source).indexOf(itemToFind);
        
        int i = 0; 
        
        for(T item : source)
            if(item.equals(itemToFind))
                return i;
            else i++;
        
        return -1;
    }

    public static <T> int lastIndexOf(Iterable<T> source, T itemToFind)
    {
        if(source instanceof ArrayList)
            return ((ArrayList<T>)source).lastIndexOf(itemToFind);
        
        int i = -1;
        int temp = 0;
        
        for(T item : source)
            if(item.equals(itemToFind))
                i = temp;
            else temp++;
        
        return i;
    }

    public static <T> Collection<T> reverse(Iterable<T> source)
    {
        return fold(source, new Stack<T>(), 
            (acc, x) -> { acc.add(x); return acc; });
    }
    
    public static <T> ArrayList<T> toArrayList(Iterable<T> source)
    {
        return source instanceof ArrayList 
            ? (ArrayList<T>)source 
            : fold(source, new ArrayList<T>(), 
                (acc, x) -> { acc.add(x); return acc; });
    }
}